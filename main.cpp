#include <string>
#include <iostream>

#include <vtkThreshold.h>
#include <vtkSmartPointer.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>

#include <vtkStringArray.h>
#include <vtkTable.h>
#include <vtkFieldData.h>
#include <vtkDelimitedTextReader.h>
#include <vtkCellData.h>

#include <ttkCinemaWriter.h>
#include <ttkCinemaQuery.h>
#include <ttkCinemaReader.h>
#include <ttkCinemaImaging.h>
#include <ttkCinemaProductReader.h>
#include <ttkDepthImageBasedGeometryApproximation.h>

using namespace std;

vector<pair<int, vector<vector<double>>>> triangles;

// Print triangles in a json format
void printTriangles();

// Open cinema database and reconstruct surface
vtkSmartPointer<vtkUnstructuredGrid> readCinemaDB(string, int);

// Add triangles to vtkMultiBlockDataSet
void addTriangles(vtkSmartPointer<vtkMultiBlockDataSet>, int);

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cout << "Please call application like this : ./build/cinema [path_to_cinema_database] [number of contours to read]";

  }
  string dataset = argv[1];
  int contourNum = stoi(argv[2]);

  vtkSmartPointer<vtkUnstructuredGrid> data = readCinemaDB(dataset, contourNum);

  return 0;
}

vtkSmartPointer<vtkUnstructuredGrid> readCinemaDB(string dataset, int contourNum)
{
  auto cinemaReader = vtkSmartPointer<ttkCinemaReader>::New();
  cinemaReader->SetDatabasePath(dataset);

  for (int i = 0 ; i < contourNum ; i++)
  {
    string querySql = "SELECT * FROM InputTable WHERE superID == " + to_string(i);

    auto query = vtkSmartPointer<ttkCinemaQuery>::New();
    query->SetQueryString(querySql);
    //query->SetdebugLevel_(3);
    query->SetInputConnection(cinemaReader->GetOutputPort());

    auto productReader = vtkSmartPointer<ttkCinemaProductReader>::New();
    //productReader->SetdebugLevel_(3);
    productReader->SetFilepathColumnName(0, 0, 0, 0, "FILE");
    productReader->SetInputConnection(query->GetOutputPort());

    auto depthApproximation = vtkSmartPointer<ttkDepthImageBasedGeometryApproximation>::New();
    //depthApproximation->SetdebugLevel_(3);
    depthApproximation->SetSubsampling(5);
    depthApproximation->SetDepthScalarField("Depth");
    depthApproximation->SetInputConnection(productReader->GetOutputPort());

    auto writer = vtkSmartPointer<vtkXMLMultiBlockDataWriter>::New();
    writer->SetFileName("output.vtm");
    writer->SetInputConnection(depthApproximation->GetOutputPort());
    writer->Write();

    addTriangles(depthApproximation->GetOutput(), i);
  }

  printTriangles();

  return vtkSmartPointer<vtkUnstructuredGrid>::New();
}

void printTriangles()
{
  printf("[");
  for (int i = 0 ; i < triangles.size() ; i++)
  {

    printf("{");
    printf("\"id\":%d,", triangles[i].first);


    printf("\"points\":[");
    for (int j = 0 ; j < 3 ; j++)
    {
      printf("[%f,%f,%f]", triangles[i].second[j][0], triangles[i].second[j][1], triangles[i].second[j][2]);
      if (j != 2)
      {
        printf(",");
      }
    }
    printf("]");
    printf("}");
    if (i != triangles.size() - 1)
    {
      printf(",");
    }
  }
  printf("]");

}

void addTriangles(vtkSmartPointer<vtkMultiBlockDataSet> mesh, int id)
{
  for (int k = 0 ; k < mesh->GetNumberOfBlocks() ; k++)
  {
    vtkSmartPointer<vtkDataObject> block = mesh->GetBlock(k);
    vtkSmartPointer<vtkUnstructuredGrid> ugrid = vtkUnstructuredGrid::SafeDownCast(block);

    for (vtkIdType tetraIndex = 0; tetraIndex < ugrid->GetNumberOfCells(); tetraIndex++)
    {
      vtkSmartPointer<vtkIdList> cell = vtkSmartPointer<vtkIdList>::New();
      ugrid->GetCellPoints(tetraIndex, cell);

      vector<vector<double>> triangle;

      for (vtkIdType cellIndex = 0; cellIndex < cell->GetNumberOfIds(); cellIndex++)
      {
        vtkIdType pointId = cell->GetId(cellIndex);

        double point[3];
        ugrid->GetPoints()->GetPoint(pointId, point);

        triangle.push_back({point[0], point[1], point[2]});
      }

      triangles.push_back(make_pair(id, triangle));
    }
  }
}

